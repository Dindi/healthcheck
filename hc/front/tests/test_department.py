from hc.test import BaseTestCase

from django.http import HttpRequest
from django.urls import reverse


class DepartmentTestCase(BaseTestCase):
        
    def test_department_create(self):
        self.client.login(username="alice@example.org", password="password")

        form = {"name": "Trial #1", "description": "Trial department one"}
        r = self.client.post("/departments/new", form)
        self.assertEqual(r.status_code, 302)

    def test_department_edit(self):
        self.client.login(username="alice@example.org", password="password")

        form = {"name": "Trial #1", "description": "Trial department one"}
        r = self.client.post("/departments/edit/1", form)
        self.assertEqual(r.status_code, 302)


class DepartmentPageTests(BaseTestCase):

    def test_department_page_status_code(self):
        self.client.login(username="alice@example.org", password="password")
        response = self.client.get('/departments')
        self.assertEquals(response.status_code, 301)

    def test_view_url_by_name(self):
        self.client.login(username="alice@example.org", password="password")
        response = self.client.get(reverse('hc-departments'))
        self.assertEquals(response.status_code, 200)

    def test_view_uses_correct_template(self):
        self.client.login(username="alice@example.org", password="password")
        response = self.client.get(reverse('hc-departments'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'front/department_list.html')

